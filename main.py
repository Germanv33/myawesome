

import os
import pygame

pygame.init()
pygame.mixer.init()


clock = pygame.time.Clock()
size = (900, 900)
screen = pygame.display.set_mode((size))
pygame.display.set_caption("Penta")
k = 1

music = pygame.mixer.music.load("music1.mp3")

sword = pygame.mixer.Sound("sword.wav")
death = pygame.mixer.Sound("death.wav")
bp = 0
ep = 0
b = 0
e = 0
n = 1
Epoints = 0
Bpoints = 0
game_over = False

def once_play(n):
    if n == 1:
        pygame.mixer.Sound.play(death)
        n -= 1
    return n



def del_2_points(mas, masOLD, Bpoints, Epoints):
    for i in range(20):
        for j in range(18):
            if mas[i*4][j*4] == "1" and mas[i*4][(j+1)*4] == "2" and mas[i*4][(j+2)*4] == "2" and mas[i*4][(j+3)*4] == "1" \
                    and \
                    ((masOLD[i*4][j*4] > masOLD[i*4][(j+1)*4] and masOLD[i*4][j*4] > masOLD[i*4][(j+2)*4]) or (masOLD[i*4][(j+3)*4] > masOLD[i*4][(j+1)*4] and masOLD[i*4][(j+3)*4]> masOLD[i*4][(j+2)*4])):

                pygame.mixer.Sound.play(sword)
                mas[i * 4][(j + 1) * 4] = 0
                mas[i * 4][(j + 2) * 4] = 0
                Bpoints += 2


            if mas[i*4][j*4] == "2" and mas[i*4][(j+1)*4] == "1" and mas[i*4][(j+2)*4] == "1" and mas[i*4][(j+3)*4] == "2" \
                    and ((masOLD[i*4][j*4] > masOLD[i*4][(j+1)*4] and masOLD[i*4][j*4] > masOLD[i*4][(j+2)*4]) or (masOLD[i*4][(j+3)*4] > masOLD[i*4][(j+1)*4] and masOLD[i*4][(j+3)*4]> masOLD[i*4][(j+2)*4])):

                pygame.mixer.Sound.play(sword)
                mas[i * 4][(j + 1) * 4] = 0
                mas[i * 4][(j + 2) * 4] = 0
                Epoints += 2


    for i in range(20):
        for j in range(18):
            if mas[j*4][i*4] == "1" and mas[(j+1)*4][i*4] == "2" and mas[(j+2)*4][i*4] == "2" and mas[(j+3)*4] [i*4]== "1" and \
                    ((masOLD[j*4][i*4] > masOLD[(j+1)*4][i*4] and masOLD[j*4][i*4] > masOLD[(j+2)*4][i*4]) or (masOLD[(j+3)*4][i*4] > masOLD[(j+1)*4][i*4] and masOLD[(j+3)*4][i*4]> masOLD[(j+2)*4][i*4])):

                pygame.mixer.Sound.play(sword)
                mas[(j + 1) * 4][i * 4] = 0
                mas[(j + 2) * 4][i * 4] = 0
                Bpoints += 2


            if mas[j * 4][i * 4] == "2" and mas[(j + 1) * 4][i * 4] == "1" and mas[(j + 2) * 4][i * 4] == "1" and \
                    mas[(j + 3) * 4][i * 4] == "2" and ((masOLD[j * 4][i * 4] > masOLD[(j + 1) * 4][i * 4] and
                                                         masOLD[j * 4][i * 4] > masOLD[(j + 2) * 4][i * 4]) or (
                                                                masOLD[(j + 3) * 4][i * 4] > masOLD[(j + 1) * 4][
                                                            i * 4] and masOLD[(j + 3) * 4][i * 4] > masOLD[(j + 2) * 4][
                                                                    i * 4])):
                pygame.mixer.Sound.play(sword)
                mas[(j + 1) * 4][i * 4] = 0
                mas[(j + 2) * 4][i * 4] = 0
                Epoints += 2


    for i in range(17):
        for j in range(17):

            if mas[i*4][j*4] == "2" and mas[(i+1)*4][(j+1)*4] == "1" and mas[(i+2)*4][(j+2)*4] == "1" and mas[(i+3)*4][(j+3)*4] == "2" and  ((masOLD[i*4][j*4] > masOLD[(i+1)*4][(j+1)*4] and masOLD[i*4][j*4] > masOLD[(i+2)*4][(j+2)*4]) or (masOLD[(i+3)*4][(j+3)*4] > masOLD[(i+1)*4][(j+1)*4] and masOLD[(i+3)*4][(j+3)*4]> masOLD[(i+2)*4][(j+2)*4])):
                    mas[(i + 1) * 4][(j+1) * 4] = 0
                    mas[(i + 2) * 4][(j+2) * 4] = 0
                    pygame.mixer.Sound.play(sword)
                    Epoints += 2


            if mas[i*4][j*4] == "1" and mas[(i+1)*4][(j+1)*4] == "2" and mas[(i+2)*4][(j+2)*4] == "2" and mas[(i+3)*4][(j+3)*4] == "1" and  ((masOLD[i*4][j*4] > masOLD[(i+1)*4][(j+1)*4] and masOLD[i*4][j*4] > masOLD[(i+2)*4][(j+2)*4]) or (masOLD[(i+3)*4][(j+3)*4] > masOLD[(i+1)*4][(j+1)*4] and masOLD[(i+3)*4][(j+3)*4]> masOLD[(i+2)*4][(j+2)*4])):
                    pygame.mixer.Sound.play(sword)
                    mas[(i + 1) * 4][(j+1) * 4] = 0
                    mas[(i + 2) * 4][(j+2) * 4] = 0
                    Bpoints += 2








    for i in range(3, 20):
        for j in range(17):

            if mas[i*4][j*4] == "1" and mas[(i-1)*4][(j+1)*4] == "2" and mas[(i-2)*4][(j+2)*4] == "2" and mas[(i-3)*4][(j+3)*4] == "1" and ((masOLD[i*4][j*4] > masOLD[(i-1)*4][(j+1)*4] and masOLD[i*4][j*4] > masOLD[(i-2)*4][(j+2)*4]) or (masOLD[(i-3)*4][(j+3)*4] > masOLD[(i-1)*4][(j+1)*4] and masOLD[(i-3)*4][(j+3)*4]> masOLD[(i-2)*4][(j+2)*4])):
                    pygame.mixer.Sound.play(sword)
                    mas[(i - 1) * 4][(j+1) * 4] = 0
                    mas[(i - 2) * 4][(j+2) * 4] = 0
                    Bpoints += 2


            if mas[i*4][j*4] == "2" and mas[(i-1)*4][(j+1)*4] == "1" and mas[(i-2)*4][(j+2)*4] == "1" and mas[(i-3)*4][(j+3)*4] == "2" and ((masOLD[i*4][j*4] > masOLD[(i-1)*4][(j+1)*4] and masOLD[i*4][j*4] > masOLD[(i-2)*4][(j+2)*4]) or (masOLD[(i-3)*4][(j+3)*4] > masOLD[(i-1)*4][(j+1)*4] and masOLD[(i-3)*4][(j+3)*4]> masOLD[(i-2)*4][(j+2)*4])):
                    pygame.mixer.Sound.play(sword)
                    mas[(i - 1) * 4][(j+1) * 4] = 0
                    mas[(i - 2) * 4][(j+2) * 4] = 0
                    Epoints += 2


    return (Bpoints,Epoints)





def check_win( mas, sign):
    for i in range(20):
        for j in range(16):
            if mas[i*4][j*4] == sign and mas[i*4][(j+1)*4] == sign and mas[i*4][(j+2)*4] == sign and mas[i*4][(j+3)*4] == sign and mas[i*4][(j+4)*4] == sign:
                return sign
    for i in range(20):
        for j in range(16):
            if mas[j*4][i*4] == sign and mas[(j+1)*4][i*4] == sign and mas[(j+2)*4][i*4] == sign and mas[(j+3)*4][i*4] == sign and mas[(j+4)*4][i*4] == sign:
                return sign

    for i in range(16):
        for j in range(16):
            win = True
            ch = mas[i*4][j*4]

            for n in range(1, 5):
                if mas[i*4 + 4*n][j*4 + 4*n] != ch:
                    win = False
                    break

            if win:
                if ch == "1" or ch == "2":
                        return sign



    for i in range(4, 20):
       for j in range(16):
           win = True
           ch = mas[i * 4][j * 4]

           for n in range(1, 5):
               if mas[i * 4 - 4*n][j * 4 + 4*n] != ch:
                   win = False
                   break

           if win:
               if ch == "1" or ch == "2":
                    return sign










FPS =60
BLACK = (0, 0, 0)
WHITE = (255,255,255)
EMERALD = (12,243,255)
font = pygame.font.SysFont('jokerman', 32)
font1 = pygame.font.Font("EH.TTF", 32)
penta_color = (132, 112, 255)

mas = [[0] * 80 for i in range(80)]
masOLD = [[0] * 80 for i in range(80)]
query = 0
balls = 0


# ------- Надпись Penta ------------

penta_hight_name = font1.render("PENTA "*20,0,penta_color,)
x_penta_move = 0.5
penta_width, penta_hight = penta_hight_name.get_size()
x_penta, y_penta = 450 - (penta_width // 2), 10

# ------------------------------------
delta_x = 70
delta_y = 20 + penta_hight + 20
margin = 700 // 19 - 29
# -------------- game zone -----------------
zone_game = pygame.Surface((700, 700))
zone_game.fill(WHITE)
screen.blit(zone_game, (0, 20 + penta_hight + 20))
# ------------------------------------------
if not game_over:
    pygame.mixer.music.play(-1)

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            quit()

        elif event.type == pygame.MOUSEBUTTONDOWN and not game_over:

            x_mouse, y_mouse = pygame.mouse.get_pos()
            if x_mouse >= delta_x and x_mouse <= delta_x + 780 and  y_mouse >= delta_y  and y_mouse <= delta_y + 780:
                if balls == 0:
                    if x_mouse >= 343 + delta_x and x_mouse <= 386 + delta_x and  y_mouse >= 346 + delta_y  and y_mouse <= 386 + delta_y:
                        print(x_mouse - delta_x, y_mouse - delta_y)
                        column = (x_mouse - delta_x) // 10
                        roww = (y_mouse - delta_y) // 10
                        if mas[roww][column] == 0:
                            if (query % 2) == 0:
                                mas[roww][column] = "1"

                            else:
                                mas[roww][column] = "2"
                            if (roww % 4) == 0 and (column % 4) == 0:
                                query += 1
                                balls += 1
                                masOLD[roww][column] += balls



                elif balls == 2:
                    if not (x_mouse >= 263 + delta_x and x_mouse <= 464 + delta_x and y_mouse >= 263 + delta_y and y_mouse <= 465 + delta_y):

                        print(x_mouse - delta_x, y_mouse - delta_y)
                        column = (x_mouse - delta_x) // 10
                        roww = (y_mouse - delta_y) // 10
                        if mas[roww][column] == 0:
                            if (query % 2) == 0:
                                mas[roww][column] = "1"

                            else:
                                mas[roww][column] = "2"
                            if (roww % 4) == 0 and (column % 4) == 0:
                                query += 1
                                balls += 1
                                masOLD[roww][column] += balls










                else:
                    print(x_mouse - delta_x,y_mouse - delta_y)
                    column = (x_mouse - delta_x) // 10
                    roww = (y_mouse - delta_y) // 10
                    if mas[roww][column] == 0:
                        if (query % 2) == 0:
                            mas[roww][column] = "1"

                        else:
                            mas[roww][column] = "2"
                        if (roww % 4) == 0 and (column % 4) == 0:
                            query += 1
                            balls += 1
                            masOLD[roww][column] += balls

        elif event.type == pygame.KEYDOWN and event.key == pygame.K_SPACE and game_over:
            mas = [[0] * 80 for i in range(80)]
            masOLD = [[0] * 80 for i in range(80)]
            balls = 0
            query = 0
            Bpoints = 0
            Epoints = 0
            bp = 0
            ep = 0
            n = 1

        elif event.type == pygame.KEYUP:
            if event.key == pygame.K_1:
                pygame.mixer.music.pause()

            elif event.key == pygame.K_2:
                pygame.mixer.music.unpause()

                pygame.mixer.music.set_volume(0.10)
            elif event.key == pygame.K_3:
                pygame.mixer.music.unpause()
                k = k * 1.25
                pygame.mixer.music.set_volume(0.5)



    clock.tick(FPS)

    screen.fill(BLACK)
    # -------------- game zone -----------------
    zone_game = pygame.Surface((766, 766))
    zone_game.fill(WHITE)
    screen.blit(zone_game, (70, 20 + penta_hight + 20))

    # ------------------------------------------
    pygame.draw.line(screen,penta_color,(0,20+penta_hight),(900,20+penta_hight),3)





# ----------- Надпись Penta --------------

    screen.blit(penta_hight_name, (x_penta, y_penta))
    x_penta += x_penta_move
    if x_penta + penta_width >450 - (penta_width // 2) + penta_width + 500:
        x_penta_move = -0.5
    if x_penta < 450 - (penta_width // 2) - 500:
        x_penta_move = 0.5

# -------------------------------------------


# ---------------- RECTANGLES ---------------------------------------------------

    k = 20 + penta_hight + 20
    for col in range(19):
        for row in range(19):
            xcc =  col*33 + (col + 1)*margin
            ycc =  row*33 + (row + 1)*margin
            xc = 70 + xcc
            yc = ycc + k
            pygame.draw.rect(screen, penta_color, (xc, yc, 33, 33))

    # ------------- 0 2 balls -----------------------
    BLUE = (202, 222, 222, 230)
    second_ball = pygame.Surface((200, 200), pygame.SRCALPHA)
    pygame.draw.rect(second_ball, BLUE, second_ball.get_rect())
    screen.blit(second_ball, (263 + delta_x, 263 + delta_y))

    zero_ball = pygame.Surface((40, 40), pygame.SRCALPHA)
    pygame.draw.rect(zero_ball, BLUE, zero_ball.get_rect())
    screen.blit(zero_ball, (343 + delta_x, 342 + delta_y))
    # ---------------------------------------------------

    margins = 33
    for cols in range(20):
        for rows in range(20):
            xcs = cols * margin + cols * margins
            ycs = rows * margin + rows * margins
            xs = 70 + xcs
            ys = ycs + k
            pygame.draw.rect(screen, BLACK, (xs, ys, margin, margin),1)
# --------------------------------------------------------------------------------
# ------------------------------- Circles --------------------------------------------
    for rows in range(20):
        for cols in range(20):
            if mas[rows*4][cols*4] == "1":
                color = BLACK

            elif mas[rows*4][cols*4] == "2":
                color = EMERALD


            else:
                color = WHITE

            xcs = cols * margin + cols * margins
            ycs = rows * margin + rows * margins
            xs = 70 + xcs
            ys = ycs + k
            pygame.draw.circle(screen, color, (xs + margin // 2 , ys + margin // 2), margin + 2)


 # ---------------------------------------------------------------------------

# --------------- Query -------------------------------------------------------

    if (query % 2) == 0:
        word = "BLACK"
    else:
        word = "EMERALD"
    QUERY_TO_HOD = font1.render(word, 0, WHITE, )
    screen.blit(QUERY_TO_HOD, (740, 850))

# ---------------------------------------------------------------------------


    if b > 0 or e > 0:
        bp += b
        b = 0
        ep += e
        e = 0
    b, e = del_2_points(mas, masOLD, Bpoints, Epoints)

    BLACKP = font1.render("Black", 0, penta_color, )
    screen.blit(BLACKP, (100, 850))
    EMERALDP = font1.render("Emerald", 0, penta_color, )
    screen.blit(EMERALDP, (300, 850))
    BLACKPOints = font1.render(str(bp), 0, penta_color, )
    screen.blit(BLACKPOints, (210, 850))
    EMERALDPOints = font1.render(str(ep), 0, penta_color, )
    screen.blit(EMERALDPOints, (450, 850))







# ----------------------------------- Game over -----------------------------------------
    if (query - 1) % 2 == 0:
        game_over = check_win(mas,"1")

    else:
        game_over = check_win(mas, "2")


    if bp == 10:
        game_over = "1"

    elif ep == 10:
        game_over = "2"



    if game_over == "1":
        if n == 1:
            pygame.mixer.Sound.play(death)
            n = 0
        screen.fill(BLACK)
        image1 = pygame.image.load("black.jpg").convert_alpha()
        screen.blit(image1, (0, 0))
        space_to_replay = font1.render("Press SPACE to Replay",0,penta_color,)
        screen.blit(space_to_replay, (250, 800))

        winner = font1.render("Black Samuray - Winner", 0, penta_color, )
        screen.blit(winner, (250, 50))
    elif game_over == "2":
        if n == 1:
            pygame.mixer.Sound.play(death)
            n = 0

        screen.fill(BLACK)
        image2 = pygame.image.load("emerald.jpg").convert_alpha()
        screen.blit(image2, (0, 0))
        space_to_replay = font1.render("Press SPACE to Replay", 0, WHITE, )
        screen.blit(space_to_replay, (250, 800))
        winner = font1.render("Emerald Mage - Winner", 0,WHITE, )
        screen.blit(winner, (250, 50))

# --------------------------------------------------------------------------------------------




    pygame.display.update()



